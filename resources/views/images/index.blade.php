@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1 class="mb-4" style="text-align: center">Images</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mb-4">
            <form action="{{ route('images.upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="images" class="form-label">Select Images (up to 5):</label>
                    <input type="file" class="form-control" name="images[]" id="images" multiple required>
                    <div class="form-text">Please select at least one image.</div>
                </div>
                <button type="submit" class="btn btn-primary">Upload</button>
            </form>
        </div>
        @if(session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
        <div class="mb-4" style="text-align: end">
            <a href="{{ route('images.download') }}" class="btn btn-success">Download All Images</a>
        </div>
        <div class="mb-4">
            <a class="btn btn-secondary" href="{{ route('images.index', ['sort' => 'latest']) }}">Сортировать по последним</a> |
            <a class="btn btn-secondary" href="{{ route('images.index', ['sort' => 'name']) }}">Сортировать по имени</a>
        </div>
        @if($images->isNotEmpty())
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-3 mb-3">
                        <div class="card">
                            <a href="{{ asset('/images/' . $image->filename) }}" target="_blank">
                                <img src="{{ asset('/images/' . $image->filename) }}" class="card-img-top" alt="Image">
                            </a>
                            <div class="card-body">
                                <p class="card-text">Имя файла: {{ $image->filename }}</p>
                                <p class="card-text">Загружено: {{ $image->created_at->format('Y-m-d H:i:s') }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <p>Изображения не найдены.</p>
        @endif

    </div>
@endsection

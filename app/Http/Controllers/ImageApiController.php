<?php

namespace App\Http\Controllers;


use App\Models\Image;
use Illuminate\Http\JsonResponse;

class ImageApiController extends Controller
{
    /**
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $images = Image::all();

        return response()->json($images);
    }

    /**
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $image = Image::find($id);

        if (!$image) {
            return response()->json(['error' => 'Изображение не найдено'], 404);
        }

        return response()->json($image);
    }
}

<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;
use Illuminate\Support\Str;
use ZipArchive;
use Illuminate\Http\UploadedFile;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImageController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $sort = request()->query('sort', 'latest');

        $images = $this->getSortedImages($sort);

        return view('images.index', compact('images'));
    }

    private function getSortedImages($sort)
    {
        $query = Image::query();

        if ($sort === 'name') {
            $query->orderBy('filename');
        } else {
            $query->latest();
        }

        return $query->get();
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function upload(Request $request): RedirectResponse
    {
        $request->validate([
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif',
            'images'   => 'max:5',
        ]);

        $uploadedFiles = [];

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $uploadedFile = $this->storeImage($image);
                if ($uploadedFile) {
                    $uploadedFiles[] = $uploadedFile;
                    Image::create([
                        'filename' => $uploadedFile,
                    ]);
                }
            }
        }

        if (count($uploadedFiles) > 0) {
            return redirect()->back()->with('success', 'Изображения успешно загружены.');
        }

        return redirect()->back()->with('error', 'Пожалуйста, выберите хотя бы одно изображение для загрузки.');
    }

    /**
     *
     * @param UploadedFile $image
     *
     * @return string|null
     */
    private function storeImage(UploadedFile $image): ?string
    {
        $originalName   = $image->getClientOriginalName();
        $filename       = Str::lower(Str::slug(pathinfo($originalName, PATHINFO_FILENAME)));
        $extension      = $image->getClientOriginalExtension();
        $uniqueFilename = $this->getUniqueFilename($filename, $extension);
        $imagePath      = public_path('images') . '/' . $uniqueFilename;

        while (file_exists($imagePath)) {
            $uniqueFilename = $this->getUniqueFilename($filename . '_' . Str::random(5), $extension);
            $imagePath      = public_path('images') . '/' . $uniqueFilename;
        }

        if ($image->move(public_path('images'), $uniqueFilename)) {
            return $uniqueFilename;
        }

        return null;
    }

    /**
     * @param string $filename
     * @param string $extension
     *
     * @return string
     */
    private function getUniqueFilename(string $filename, string $extension): string
    {
        $fullFilename = $filename . '.' . $extension;
        $counter      = 0;

        while (Storage::exists('images/' . $fullFilename)) {
            $counter++;
            $fullFilename = $filename . '_' . $counter . '.' . $extension;
        }

        return $fullFilename;
    }

    /**
     * @return BinaryFileResponse
     */
    public function download(): BinaryFileResponse
    {
        $files       = Image::pluck('filename')->toArray();
        $zipFileName = 'images.zip';

        $zip = new ZipArchive();
        if ($zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE) === true) {
            foreach ($files as $file) {
                $filePath = public_path('images/') . $file;
                if (file_exists($filePath)) {
                    $zip->addFile($filePath, $file);
                }
            }
            $zip->close();
        }

        return response()->download($zipFileName);
    }
}
